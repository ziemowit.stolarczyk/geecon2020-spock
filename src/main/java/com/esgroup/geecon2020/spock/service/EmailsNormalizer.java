package com.esgroup.geecon2020.spock.service;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;

import java.util.List;
import java.util.stream.Collectors;

public class EmailsNormalizer {

  public List<String> normalize(List<String> potentialEmails) {
    if(potentialEmails == null) {
      return List.of();
    }

    return potentialEmails.stream()
                          .filter(StringUtils::isNotBlank)
                          .map(StringUtils::trim)
                          .filter(GenericValidator::isEmail)
                          .map(StringUtils::lowerCase)
                          .distinct()
                          .collect(Collectors.toList());
  }

}
