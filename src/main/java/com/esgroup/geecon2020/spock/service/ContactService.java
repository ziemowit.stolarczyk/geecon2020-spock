package com.esgroup.geecon2020.spock.service;

import com.esgroup.geecon2020.spock.repository.ContactRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ContactService {

  private final ContactRepository contactRepo;
  private final EmailsNormalizer emailsNormalizer;

}
