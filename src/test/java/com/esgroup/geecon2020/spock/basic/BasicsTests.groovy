package com.esgroup.geecon2020.spock.basic

import spock.lang.Specification

class BasicsTests extends Specification {

  def '''The most basic test in Spock.'''() {
    expect:
    1 + 2 == 3
  }

  def '''The most basic test in Spock in more verbose way.'''() {
    given:
    def a = 2
    def b = 1

    when:
    def result = a + b

    then:
    result == 3
  }

  def '''Where clause.'''() {
    given:
    def a = 2
    def b = 1

    when:
    def result = (a + b - c) % d

    then:
    result == expectedResult

    where:
    c | d || expectedResult
    0 | 2 || 1
    1 | 2 || 0
  }

}
